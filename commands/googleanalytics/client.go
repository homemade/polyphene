package googleanalytics

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"bitbucket.org/homemade/polyphene/api"
)

const (
	GA_MeasureAPILiveEndpoint  = "https://www.google-analytics.com/batch"
	GA_MeasureAPIDebugEndpoint = "https://www.google-analytics.com/debug/collect"
	GA_MeasureAPIVersion       = "1"
)

type Client struct {
	UserAgent   string
	ContentType string
	HttpClient  *http.Client
	DebugMode   bool
}

type BatchParams struct {
	TrackingID       string
	ClientID         string
	DocumentHostName string // Optional

	EventHitType  string
	EventCategory string
	EventAction   string
	EventLabel    string

	TransactionHitType string
	TransactionID      string
	TransactionRevenue string

	ItemHitType       string
	ItemTransactionID string
	ItemName          string
	ItemCode          string
	ItemVariation     string
	ItemPrice         string
	ItemQuantity      string
}

type BatchResponse struct {
	DebugMode bool
	Status    string
	Body      []byte
}

func (c Client) SendBatch(params BatchParams) (BatchResponse, error) {

	result := BatchResponse{DebugMode: c.DebugMode}

	// Build the batch payload
	// See https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#batch

	// First build the Event hit
	event := url.Values{}
	event.Set("v", GA_MeasureAPIVersion)  // Version
	event.Set("tid", params.TrackingID)   // Tracking ID
	event.Set("cid", params.ClientID)     // Client ID
	event.Set("t", params.EventHitType)   // Event Hit Type
	event.Set("ec", params.EventCategory) // Event Category
	event.Set("ea", params.EventAction)   // Event Action
	event.Set("el", params.EventLabel)    // Event Label
	if params.DocumentHostName != "" {
		event.Set("dh", params.DocumentHostName) // Document Host Name
	}

	// Then the Transaction hit
	trans := url.Values{}
	trans.Set("v", GA_MeasureAPIVersion)       // Version
	trans.Set("tid", params.TrackingID)        // Tracking ID
	trans.Set("cid", params.ClientID)          // Client ID
	trans.Set("t", params.TransactionHitType)  // Transaction Hit Type
	trans.Set("ti", params.TransactionID)      // Transaction ID
	trans.Set("tr", params.TransactionRevenue) // Transaction Revenue
	if params.DocumentHostName != "" {
		event.Set("dh", params.DocumentHostName) // Document Host Name
	}

	// Then the Item hit
	item := url.Values{}
	item.Set("v", GA_MeasureAPIVersion)      // Version
	item.Set("tid", params.TrackingID)       // Tracking ID
	item.Set("cid", params.ClientID)         // Client ID
	item.Set("t", params.ItemHitType)        // Item Hit Type
	item.Set("ti", params.ItemTransactionID) // Item Transaction ID
	item.Set("in", params.ItemName)          // Item Name
	item.Set("ic", params.ItemCode)          // Item Code / SKU
	item.Set("iv", params.ItemVariation)     // Item Variation / Category
	item.Set("ip", params.ItemPrice)         // Item Price
	item.Set("iq", params.ItemQuantity)      // Item Quantity
	if params.DocumentHostName != "" {
		event.Set("dh", params.DocumentHostName) // Document Host Name
	}

	// Finally Seperate with new lines to build the batch payload
	payload := fmt.Sprintf("%s\n%s\n%s", event.Encode(), trans.Encode(), item.Encode())

	// Set endpoint
	endpoint := GA_MeasureAPILiveEndpoint
	if c.DebugMode {
		endpoint = GA_MeasureAPIDebugEndpoint
	}

	method := "POST"
	apiReq, err := api.BuildRequest(c.UserAgent, "*/*", method, endpoint, strings.NewReader(payload))
	if err != nil {
		return result, err
	}
	var apiRes *http.Response
	var apiResBody []byte
	apiRes, apiResBody, err = api.Do(c.HttpClient, apiReq)
	if err != nil {
		return result, err
	}
	result.Status = apiRes.Status
	result.Body = apiResBody
	if apiRes.StatusCode != http.StatusOK {
		return result, fmt.Errorf("status %s returned from api call to %s", apiRes.Status, endpoint)
	}

	return result, nil

}
