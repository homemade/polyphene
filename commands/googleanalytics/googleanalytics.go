package googleanalytics

import (
	"bitbucket.org/homemade/polyphene/commands"
	"bitbucket.org/homemade/polyphene/providers"
)

var (
	ConfigKey_DebugMode = providers.GoogleAnalytics.String() + ".DebugMode"
	ConfigKey_Params    = providers.GoogleAnalytics.String() + commands.ConfigKeySuffix_Params
	ConfigKey_Result    = providers.GoogleAnalytics.String() + commands.ConfigKeySuffix_Result
)
