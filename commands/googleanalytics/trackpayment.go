package googleanalytics

import (
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/api"
	"bitbucket.org/homemade/polyphene/mapping"
)

type TrackPayment struct {
	client                *Client
	paramsMappingTemplate mapping.MappingTemplate
	mapResults            bool // result mapping is optional
	resultMappingTemplate mapping.MappingTemplate
}

func (t *TrackPayment) Configure(cfg map[string]string) error {

	t.client = &Client{
		UserAgent:   "Polyphene",
		ContentType: "application/javascript",
		HttpClient:  api.DefaultHTTPClient(),
	}

	// optional debug mode
	if debugMode, ok := cfg[ConfigKey_DebugMode]; ok {
		if debugMode == "true" {
			t.client.DebugMode = true
		}
	}

	// create toml mapping template for unmarshalling params
	var params []string
	var err error
	params, err = polyphene.GetRequiredConfigValues(cfg, ConfigKey_Params)
	if err != nil {
		return err
	}
	t.paramsMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Params, params[0])
	if err != nil {
		return err
	}
	// result mapping is optional
	if r, ok := cfg[ConfigKey_Result]; ok && r != "" {
		t.resultMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Result, r)
		if err != nil {
			return err
		}
		t.mapResults = true
	}
	return nil
}

func (t TrackPayment) Invoke(event polyphene.Event) (polyphene.Outcome, error) {
	var params BatchParams
	_, err := t.paramsMappingTemplate.ExecuteAndUnmarshal(event.ModelValues(), &params)
	if err != nil {
		return nil, err
	}
	var recoverableErrors []string
	var response BatchResponse
	result := make(map[string]interface{})
	response, err = t.client.SendBatch(params)
	if err != nil {
		return nil, fmt.Errorf("send batch %#v failed with error %v", params, err)
	}
	if t.mapResults {
		_, err = t.resultMappingTemplate.ExecuteAndUnmarshal(response, &result)
		if err != nil {
			return nil, err
		}
	}
	return polyphene.CreateOutcome(result, recoverableErrors), nil
}
