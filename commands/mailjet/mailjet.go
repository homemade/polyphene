package mailjet

import (
	"bitbucket.org/homemade/polyphene/commands"
	"bitbucket.org/homemade/polyphene/providers"
)

var (
	ConfigKey_PublicAPIKey  = providers.Mailjet.String() + ".PublicAPIKey"
	ConfigKey_PrivateAPIKey = providers.Mailjet.String() + ".PrivateAPIKey"
	ConfigKey_Params        = providers.Mailjet.String() + commands.ConfigKeySuffix_Params
	ConfigKey_Result        = providers.Mailjet.String() + commands.ConfigKeySuffix_Result
)
