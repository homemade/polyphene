package mailjet

import (
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/api"
	"bitbucket.org/homemade/polyphene/mapping"

	mailjetApi "github.com/mailjet/mailjet-apiv3-go"
)

type SendTemplatedEmail struct {
	client                *mailjetApi.Client
	paramsMappingTemplate mapping.MappingTemplate
	mapResults            bool // result mapping is optional
	resultMappingTemplate mapping.MappingTemplate
}

func (s *SendTemplatedEmail) Configure(cfg map[string]string) error {
	// to create a mailjet client we require a public and private api key
	apiKeys, err := polyphene.GetRequiredConfigValues(cfg, ConfigKey_PublicAPIKey, ConfigKey_PrivateAPIKey)
	if err != nil {
		return err
	}
	s.client = mailjetApi.NewMailjetClient(apiKeys[0], apiKeys[1])
	s.client.SetClient(api.DefaultHTTPClient())
	// create toml mapping template for unmarshalling params
	var params []string
	params, err = polyphene.GetRequiredConfigValues(cfg, ConfigKey_Params)
	if err != nil {
		return err
	}
	s.paramsMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Params, params[0])
	if err != nil {
		return err
	}
	// result mapping is optional
	if v, ok := cfg[ConfigKey_Result]; ok && v != "" {
		s.resultMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Result, v)
		if err != nil {
			return err
		}
		s.mapResults = true
	}
	return nil
}

func (s SendTemplatedEmail) Invoke(event polyphene.Event) (polyphene.Outcome, error) {
	var sendMailParams mailjetApi.InfoSendMail
	_, err := s.paramsMappingTemplate.ExecuteAndUnmarshal(event.ModelValues(), &sendMailParams)
	if err != nil {
		return nil, err
	}
	var recoverableErrors []string
	var response *mailjetApi.SentResult
	response, err = s.client.SendMail(&sendMailParams)
	if err != nil {
		return nil, fmt.Errorf("send mail %#v failed with error %v", sendMailParams, err)
	}
	result := make(map[string]interface{})
	if s.mapResults {
		_, err = s.resultMappingTemplate.ExecuteAndUnmarshal(response, &result)
		if err != nil {
			return nil, err
		}
	}
	return polyphene.CreateOutcome(result, recoverableErrors), nil
}
