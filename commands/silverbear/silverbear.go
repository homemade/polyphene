package silverbear

import (
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/clbanning/mxj"

	"bitbucket.org/homemade/polyphene/api"
	"bitbucket.org/homemade/polyphene/commands"
	"bitbucket.org/homemade/polyphene/providers"
	"bitbucket.org/homemade/polyphene/templates/silverbear"
)

var (
	ConfigKey_SilverbearEndpoint = providers.Silverbear.String() + ".Endpoint"
	ConfigKey_SilverbearUsername = providers.Silverbear.String() + ".Username"
	ConfigKey_SilverbearPassword = providers.Silverbear.String() + ".Password"

	// ConfigKey_SilverbearGUIDPrdDonReg = providers.Silverbear.String() + ".GUIDPrdDonReg"
	// ConfigKey_SilverbearGUIDPlsDonReg = providers.Silverbear.String() + ".GUIDPlsDonReg"

	// ConfigKey_SilverbearGUIDPrdDonSin = providers.Silverbear.String() + ".GUIDPrdDonSin"
	// ConfigKey_SilverbearGUIDPlsDonSin = providers.Silverbear.String() + ".GUIDPlsDonSin"

	ConfigKey_SilverbearGUIDCur    = providers.Silverbear.String() + ".GUIDCur"
	ConfigKey_SilverbearGUIDBusOrg = providers.Silverbear.String() + ".GUIDBusOrg"

	ConfigKey_Params = providers.Silverbear.String() + commands.ConfigKeySuffix_Params
	ConfigKey_Result = providers.Silverbear.String() + commands.ConfigKeySuffix_Result
)

type Client struct {
	UserAgent                  string
	HTTPClient                 *http.Client
	SilverbearEndpoint         string
	SilverbearUsername         string
	SilverbearPassword         string
	SilverbearRequestTemplates map[string]api.RequestTemplate
}

type ContactDetails struct {
	Title        string
	FirstName    string
	LastName     string
	EmailAddress string
	PhoneNumber  string
	Address1     string
	Address2     string
	Address3     string
	TownOrCity   string
	Postcode     string
}

type BankDetails struct {
	AccountName      string
	AccountNumber    string
	SortCode         string
	BankName         string
	BankAddressLine1 string
	BankAddressLine2 string
	BankAddressCity  string
	BankPostcode     string
}

type DonateByDirectDebitParams struct {
	ContactDetails ContactDetails
	BankDetails    BankDetails
	Amount         string
}

type DirectDebit struct {
	InvoiceGUID string
	SBContact   SBContact
	BACSRef     string
}

type SBContact struct {
	GUID           string
	ContactDetails ContactDetails
}

func (c *Client) SetRequestTemplates(useragent string, endpoint string, username string, password string, config map[string]string) error {
	var err error
	c.SilverbearRequestTemplates, err = silverbear.BuildRequestTemplates(useragent, endpoint, username, password, config)
	if err != nil {
		return err
	}
	return nil
}

func (c Client) FindContactsByEmail(emailaddress string) ([]SBContact, error) {
	req := c.SilverbearRequestTemplates[silverbear.Action_FindContactsByEmail]
	apiReq, reqBody, err := api.BuildTemplatedRequest(req, struct{ EmailAddress string }{emailaddress})
	if err != nil {
		return nil, err
	}
	res, resBody, err := api.Do(c.HTTPClient, apiReq)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("status %s returned from api call to %s with request %s and response %s", res.Status, req.URL, string(reqBody), string(resBody))
	}
	err = api.CheckContentType(req.ContentType, res.Header["Content-Type"])
	if err != nil {
		return nil, err
	}
	//use mxj to read the xml
	//see https://github.com/clbanning/mxj
	var xmlMap mxj.Map
	xmlMap, err = mxj.NewMapXml(resBody)
	if err != nil {
		return nil, fmt.Errorf("error parsing xml response %v", err)
	}
	var contacts []SBContact
	var contactsXML []interface{}
	contactsXML, err = xmlMap.ValuesForPath("Envelope.Body.FindContactsByEmailResponse.FindContactsByEmailResult.SBContact")
	if err != nil {
		return nil, fmt.Errorf("error parsing contacts %v", err)
	}
	for i, c := range contactsXML {
		cv, ok := c.(map[string]interface{})
		if !ok {
			return nil, fmt.Errorf("contact at index %d is an invalid type %s", i, reflect.TypeOf(cv))
		}
		contactMap := mxj.Map(cv)
		contact := SBContact{
			GUID: contactMap.ValueOrEmptyForPathString("ContactDetails.ContactId"),
			ContactDetails: ContactDetails{
				Title:       contactMap.ValueOrEmptyForPathString("ContactDetails.Title"),
				FirstName:   contactMap.ValueOrEmptyForPathString("ContactDetails.FirstName"),
				LastName:    contactMap.ValueOrEmptyForPathString("ContactDetails.LastName"),
				PhoneNumber: contactMap.ValueOrEmptyForPathString("ContactDetails.HomePhone"),
			},
		}
		var emailAddressesXML []interface{}
		emailAddressesXML, err = contactMap.ValuesForPath("ContactDetails.EmailAddresses.EmailAddress")
		if err != nil {
			return nil, fmt.Errorf("error parsing email adresses %v", err)
		}
		for i, e := range emailAddressesXML {
			ev, ok := e.(map[string]interface{})
			if !ok {
				return nil, fmt.Errorf("email address at index %d is an invalid type %s", i, reflect.TypeOf(ev))
			}
			emailAddressMap := mxj.Map(ev)
			if emailAddressMap.ValueOrEmptyForPathString("Type") == "Home" {
				contact.ContactDetails.EmailAddress = emailAddressMap.ValueOrEmptyForPathString("Email")
				break
			}
		}
		var addressesXML []interface{}
		addressesXML, err = contactMap.ValuesForPath("ContactDetails.Addresses.Address")
		if err != nil {
			return nil, fmt.Errorf("error parsing adresses %v", err)
		}
		for i, a := range addressesXML {
			av, ok := a.(map[string]interface{})
			if !ok {
				return nil, fmt.Errorf("address at index %d is an invalid type %s", i, reflect.TypeOf(av))
			}
			addressMap := mxj.Map(av)
			if addressMap.ValueOrEmptyForPathString("Type") == "Home" {
				contact.ContactDetails.Address1 = addressMap.ValueOrEmptyForPathString("Address1")
				contact.ContactDetails.Address2 = addressMap.ValueOrEmptyForPathString("Address2")
				contact.ContactDetails.Address3 = addressMap.ValueOrEmptyForPathString("Address3")
				contact.ContactDetails.TownOrCity = addressMap.ValueOrEmptyForPathString("City")
				contact.ContactDetails.Postcode = addressMap.ValueOrEmptyForPathString("PostalCode")
				break
			}
		}
		contacts = append(contacts, contact)
	}
	return contacts, nil
}

func (c Client) MatchContact(search ContactDetails, contacts []SBContact) int {
	lt := func(s string) string {
		return strings.ToLower(strings.TrimSpace(s))
	}
	for i, c := range contacts {
		if lt(search.Title) == lt(c.ContactDetails.Title) &&
			lt(search.FirstName) == lt(c.ContactDetails.FirstName) &&
			lt(search.LastName) == lt(c.ContactDetails.LastName) &&
			lt(search.EmailAddress) == lt(c.ContactDetails.EmailAddress) &&
			lt(search.PhoneNumber) == lt(c.ContactDetails.PhoneNumber) &&
			lt(search.Address1) == lt(c.ContactDetails.Address1) &&
			lt(search.Address2) == lt(c.ContactDetails.Address2) &&
			lt(search.Address3) == lt(c.ContactDetails.Address3) &&
			lt(search.TownOrCity) == lt(c.ContactDetails.TownOrCity) &&
			lt(search.Postcode) == lt(c.ContactDetails.Postcode) {
			return i
		}
	}
	return -1
}

func (c Client) RegisterUser(user ContactDetails) (SBContact, error) {
	var result SBContact
	req := c.SilverbearRequestTemplates[silverbear.Action_RegisterUser]
	apiReq, reqBody, err := api.BuildTemplatedRequest(req, user)
	if err != nil {
		return result, err
	}
	res, resBody, err := api.Do(c.HTTPClient, apiReq)
	if err != nil {
		return result, err
	}
	if res.StatusCode != http.StatusOK {
		return result, fmt.Errorf("status %s returned from api call to %s with request %s and response %s", res.Status, req.URL, string(reqBody), string(resBody))
	}
	err = api.CheckContentType(req.ContentType, res.Header["Content-Type"])
	if err != nil {
		return result, err
	}
	//use mxj to read the xml
	//see https://github.com/clbanning/mxj
	var xmlMap mxj.Map
	xmlMap, err = mxj.NewMapXml(resBody)
	if err != nil {
		return result, fmt.Errorf("error parsing xml request %v", err)
	}
	result.ContactDetails = user
	result.GUID = xmlMap.ValueOrEmptyForPathString("Envelope.Body.RegisterUserResponse.RegisterUserResult.ContactId")
	if result.GUID == "" {
		return result, fmt.Errorf("missing Envelope.Body.RegisterUserResponse.RegisterUserResult.ContactId in response %s", string(resBody))
	}
	return result, nil
}

func (c Client) CreateDirectDebitInstruction(params DonateByDirectDebitParams, contact SBContact) (guid string, timestamp string, err error) {
	req := c.SilverbearRequestTemplates[silverbear.Action_CreateDirectDebitInstruction]
	var apiReq *http.Request
	var reqBody []byte
	data := struct {
		Params    DonateByDirectDebitParams
		SBContact SBContact
	}{
		Params:    params,
		SBContact: contact,
	}
	apiReq, reqBody, err = api.BuildTemplatedRequest(req, data)
	if err != nil {
		return "", "", err
	}
	res, resBody, err := api.Do(c.HTTPClient, apiReq)
	if err != nil {
		return "", "", err
	}
	if res.StatusCode != http.StatusOK {
		return "", "", fmt.Errorf("status %s returned from api call to %s with request %s and response %s", res.Status, req.URL, string(reqBody), string(resBody))
	}
	err = api.CheckContentType(req.ContentType, res.Header["Content-Type"])
	if err != nil {
		return "", "", err
	}
	//use mxj to read the xml
	//see https://github.com/clbanning/mxj
	var xmlMap mxj.Map
	xmlMap, err = mxj.NewMapXml(resBody)
	if err != nil {
		return "", "", fmt.Errorf("error parsing xml request %v", err)
	}
	timestamp = xmlMap.ValueOrEmptyForPathString("Envelope.Header.Security.Timestamp.Created")
	if timestamp == "" {
		return "", "", fmt.Errorf("missing Envelope.Header.Security.Timestamp.Created in response %s", string(resBody))
	}
	guid = xmlMap.ValueOrEmptyForPathString("Envelope.Body.CreateDirectDebitInstructionResponse.CreateDirectDebitInstructionResult")
	if guid == "" {
		return "", "", fmt.Errorf("missing Envelope.Body.CreateDirectDebitInstructionResponse.CreateDirectDebitInstructionResult in response %s", string(resBody))
	}
	return
}

func (c Client) GetDirectDebitInstructions(contactID string) (mxj.Map, error) {
	req := c.SilverbearRequestTemplates[silverbear.Action_GetDirectDebitInstructions]
	apiReq, reqBody, err := api.BuildTemplatedRequest(req, contactID)
	if err != nil {
		return nil, err
	}
	res, resBody, err := api.Do(c.HTTPClient, apiReq)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("status %s returned from api call to %s with request %s and response %s", res.Status, req.URL, string(reqBody), string(resBody))
	}
	err = api.CheckContentType(req.ContentType, res.Header["Content-Type"])
	if err != nil {
		return nil, err
	}
	//use mxj to read the xml
	//see https://github.com/clbanning/mxj
	var xmlMap mxj.Map
	xmlMap, err = mxj.NewMapXml(resBody)
	if err != nil {
		return nil, fmt.Errorf("error parsing xml request %v", err)
	}
	return xmlMap, nil
}

func (c Client) PurchaseProducts(params DonateByDirectDebitParams, contact SBContact, timestamp string, ddguid string) (invoiceguid string, err error) {
	req := c.SilverbearRequestTemplates[silverbear.Action_PurchaseProducts]
	var apiReq *http.Request
	var reqBody []byte
	data := struct {
		Params    DonateByDirectDebitParams
		SBContact SBContact
		Timestamp string
		DDGUID    string
	}{
		Params:    params,
		SBContact: contact,
		Timestamp: timestamp,
		DDGUID:    ddguid,
	}
	apiReq, reqBody, err = api.BuildTemplatedRequest(req, data)
	if err != nil {
		return "", err
	}
	res, resBody, err := api.Do(c.HTTPClient, apiReq)
	if err != nil {
		return "", err
	}
	if res.StatusCode != http.StatusOK {
		return "", fmt.Errorf("status %s returned from api call to %s with request %s and response %s", res.Status, req.URL, string(reqBody), string(resBody))
	}
	err = api.CheckContentType(req.ContentType, res.Header["Content-Type"])
	if err != nil {
		return "", err
	}
	//use mxj to read the xml
	//see https://github.com/clbanning/mxj
	var xmlMap mxj.Map
	xmlMap, err = mxj.NewMapXml(resBody)
	if err != nil {
		return "", fmt.Errorf("error parsing xml request %v", err)
	}
	invoiceguid = xmlMap.ValueOrEmptyForPathString("Envelope.Body.PurchaseProductsResponse.PurchaseProductsResult")
	if invoiceguid == "" {
		return "", fmt.Errorf("missing Envelope.Body.PurchaseProductsResponse.PurchaseProductsResult in response %s", string(resBody))
	}
	return invoiceguid, nil
}
