package silverbear

import (
	"fmt"
	"reflect"

	"github.com/clbanning/mxj"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/api"
	"bitbucket.org/homemade/polyphene/mapping"
)

type DonateByDirectDebit struct {
	client                *Client
	paramsMappingTemplate mapping.MappingTemplate
	mapResults            bool // result mapping is optional
	resultMappingTemplate mapping.MappingTemplate
}

func (d *DonateByDirectDebit) Configure(cfg map[string]string) error {

	keys, err := polyphene.GetRequiredConfigValues(cfg,
		ConfigKey_SilverbearEndpoint,
		ConfigKey_SilverbearUsername,
		ConfigKey_SilverbearPassword,
		ConfigKey_SilverbearGUIDCur,
		ConfigKey_SilverbearGUIDBusOrg)
	if err != nil {
		return err
	}

	d.client = &Client{
		UserAgent:          "Polyphene",
		HTTPClient:         api.DefaultHTTPClient(),
		SilverbearEndpoint: keys[0],
		SilverbearUsername: keys[1],
		SilverbearPassword: keys[2],
	}

	// add request templates
	d.client.SetRequestTemplates(d.client.UserAgent, keys[0], keys[1], keys[2], cfg)
	if err != nil {
		return err
	}

	// create toml mapping template for unmarshalling params
	var params []string
	params, err = polyphene.GetRequiredConfigValues(cfg, ConfigKey_Params)
	if err != nil {
		return err
	}
	d.paramsMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Params, params[0])
	if err != nil {
		return err
	}
	// result mapping is optional
	if r, ok := cfg[ConfigKey_Result]; ok && r != "" {
		d.resultMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Result, r)
		if err != nil {
			return err
		}
		d.mapResults = true
	}
	return nil
}

func (d DonateByDirectDebit) Invoke(event polyphene.Event) (polyphene.Outcome, error) {
	var params DonateByDirectDebitParams

	_, err := d.paramsMappingTemplate.ExecuteAndUnmarshal(event.ModelValues(), &params)
	if err != nil {
		return nil, err
	}
	var recoverableErrors []string
	var response DirectDebit
	result := make(map[string]interface{})
	response, err = donateByDirectDebit(d.client, params)
	if err != nil {
		return nil, fmt.Errorf("create direct debit %#v failed with error %v", params, err)
	}
	if d.mapResults {
		_, err = d.resultMappingTemplate.ExecuteAndUnmarshal(response, &result)
		if err != nil {
			return nil, err
		}
	}
	return polyphene.CreateOutcome(result, recoverableErrors), nil
}

func donateByDirectDebit(client *Client, params DonateByDirectDebitParams) (DirectDebit, error) {

	var result DirectDebit
	var err error

	// setting up a direct debit with silverbear involves a sequence of api calls...

	// first we look for an existing contact
	var contacts []SBContact
	contacts, err = client.FindContactsByEmail(params.ContactDetails.EmailAddress)
	if err != nil {
		return result, err
	}
	var contact SBContact

	i := client.MatchContact(params.ContactDetails, contacts)
	if i >= 0 {
		contact = contacts[i]
	}
	// if no match was found we create a new contact
	if contact.GUID == "" {
		contact, err = client.RegisterUser(params.ContactDetails)
		if err != nil {
			return result, err
		}
	}

	// next we setup the direct debit instruction
	var ddGUID string
	var timestamp string
	ddGUID, timestamp, err = client.CreateDirectDebitInstruction(params, contact)
	if err != nil {
		return result, err
	}
	var xmlMap mxj.Map
	xmlMap, err = client.GetDirectDebitInstructions(contact.GUID)
	if err != nil {
		return result, err
	}
	var ddisXML []interface{}
	ddisXML, err = xmlMap.ValuesForPath("Envelope.Body.GetDirectDebitInstructionsResponse.GetDirectDebitInstructionsResult.SBDirectDebitInstruction")
	if err != nil {
		return result, fmt.Errorf("error parsing direct debit instructions %v", err)
	}
	var bacsRef string
	for i, d := range ddisXML {
		v, ok := d.(map[string]interface{})
		if !ok {
			return result, fmt.Errorf("direct debit instruction at index %d is an invalid type %s", i, reflect.TypeOf(v))
		}
		ddiMap := mxj.Map(v)
		if ddiMap.ValueOrEmptyForPathString("Id.#text") == ddGUID {
			bacsRef = ddiMap.ValueOrEmptyForPathString("Name.#text") // bacs ref is stored in the Name element
			if bacsRef == "" {
				return result, fmt.Errorf("direct debit instruction with guid %s does not have a bacs ref", ddGUID)
			}
			break
		}
	}
	if bacsRef == "" {
		return result, fmt.Errorf("direct debit instruction with guid %s does not exist in database", ddGUID)
	}

	// commit transaction
	var invoiceGUID string
	// TODO
	result.InvoiceGUID = timestamp
	//invoiceGUID, err = client.PurchaseProducts(params, contact, timestamp, ddGUID)

	result.InvoiceGUID = invoiceGUID
	result.BACSRef = bacsRef

	return result, nil

}
