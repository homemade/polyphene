package braintree

import (
	"context"
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/api"
	"bitbucket.org/homemade/polyphene/mapping"

	braintreeApi "github.com/lionelbarrow/braintree-go"
)

type CreateTransaction struct {
	client                *braintreeApi.Braintree
	paramsMappingTemplate mapping.MappingTemplate
	mapResults            bool // result mapping is optional
	resultMappingTemplate mapping.MappingTemplate
}

func (t *CreateTransaction) Configure(cfg map[string]string) error {

	// to create a braintree client we require an environment, merchant id and public and private keys
	cfgVals, err := polyphene.GetRequiredConfigValues(cfg, ConfigKey_Environment, ConfigKey_Merchant, ConfigKey_PublicKey, ConfigKey_PrivateKey)
	if err != nil {
		return err
	}
	// convert string to braintree environment
	var env braintreeApi.Environment
	env, err = braintreeApi.EnvironmentFromName(cfgVals[0])
	if err != nil {
		return err
	}
	t.client = braintreeApi.NewWithHttpClient(
		env,
		cfgVals[1],
		cfgVals[2],
		cfgVals[3],
		api.DefaultHTTPClient(),
	)

	// create toml mapping template for unmarshalling params
	var params []string
	params, err = polyphene.GetRequiredConfigValues(cfg, ConfigKey_Params)
	if err != nil {
		return err
	}
	t.paramsMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Params, params[0])
	if err != nil {
		return err
	}

	// result mapping is optional
	if v, ok := cfg[ConfigKey_Result]; ok && v != "" {
		t.resultMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Result, v)
		if err != nil {
			return err
		}
		t.mapResults = true
	}
	return nil
}

func (t CreateTransaction) Invoke(event polyphene.Event) (polyphene.Outcome, error) {
	var createTransParams braintreeApi.TransactionRequest
	_, err := t.paramsMappingTemplate.ExecuteAndUnmarshal(event.ModelValues(), &createTransParams)
	if err != nil {
		return nil, err
	}
	var recoverableErrors []string
	var response *braintreeApi.Transaction
	ctx := context.Background()
	response, err = t.client.Transaction().Create(ctx, &createTransParams)
	if err != nil {
		if recoverable, re := IsRecoverableError(err); recoverable {
			recoverableErrors = append(recoverableErrors, re)
		} else {
			return nil, fmt.Errorf("create transaction %#v failed with error %v", createTransParams, err)
		}
	}

	result := make(map[string]interface{})
	if t.mapResults && response != nil {
		_, err = t.resultMappingTemplate.ExecuteAndUnmarshal(response, &result)
		if err != nil {
			return nil, err
		}
	}
	return polyphene.CreateOutcome(result, recoverableErrors), nil
}
