package braintree

import (
	"bitbucket.org/homemade/polyphene/commands"
	"bitbucket.org/homemade/polyphene/providers"
)

var (
	ConfigKey_Environment = providers.Braintree.String() + ".Environment"
	ConfigKey_Merchant    = providers.Braintree.String() + ".Merchant"
	ConfigKey_PublicKey   = providers.Braintree.String() + ".PublicKey"
	ConfigKey_PrivateKey  = providers.Braintree.String() + ".PrivateKey"
	ConfigKey_Params      = providers.Braintree.String() + commands.ConfigKeySuffix_Params
	ConfigKey_Result      = providers.Braintree.String() + commands.ConfigKeySuffix_Result
)
