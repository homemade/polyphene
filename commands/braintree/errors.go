package braintree

import (
	"fmt"

	"bitbucket.org/homemade/polyphene/providers"

	braintreeApi "github.com/lionelbarrow/braintree-go"
)

func IsRecoverableError(err error) (bool, string) {
	// The cleanest way to manage recoverable errors from Lionel Barrow's Go client library seems to be to check for a transaction status
	// see https://developers.braintreepayments.com/reference/general/statuses
	if be, ok := err.(*braintreeApi.BraintreeError); ok && be.Transaction != nil && be.Transaction.Status != "" {
		re := fmt.Sprintf("%s:%s|%d|%s", providers.Braintree.String(), be.Transaction.Status, be.Transaction.ProcessorResponseCode, err.Error())
		return true, re
	}
	return false, ""
}
