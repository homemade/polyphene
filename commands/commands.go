// Package commands contains various configurable polyphene commands .
package commands

const (
	ConfigKeySuffix_Params = ".Params"
	ConfigKeySuffix_Result = ".Result"
)
