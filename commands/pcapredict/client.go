package pcapredict

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/homemade/polyphene/api"
)

type Client struct {
	UserAgent   string
	ContentType string
	HttpClient  *http.Client
	APIKey      string
}

type ValidateDirectDebitParams struct {
	AccountNumber string
	SortCode      string
}

type ValidateDirectDebitResult struct {
	Items []struct {
		Error                   string
		IsCorrect               bool
		IsDirectDebitCapable    bool
		StatusInformation       string
		CorrectedSortCode       string
		CorrectedAccountNumber  string
		IBAN                    string
		Bank                    string
		BankBIC                 string
		Branch                  string
		BranchBIC               string
		ContactAddressLine1     string
		ContactAddressLine2     string
		ContactPostTown         string
		ContactPostcode         string
		ContactPhone            string
		ContactFax              string
		FasterPaymentsSupported bool
		CHAPSSupported          bool
	}
}

func (c Client) ValidateDirectDebit(params ValidateDirectDebitParams) (ValidateDirectDebitResult, error) {
	var result ValidateDirectDebitResult
	url := fmt.Sprintf("https://services.postcodeanywhere.co.uk/BankAccountValidation/Interactive/Validate/v2.00/json3.ws?Key=%s&AccountNumber=%s&SortCode=%s", c.APIKey, params.AccountNumber, params.SortCode)
	reduxedURL := strings.Replace(url, c.APIKey, "****-****-****-****", 1)
	method := "GET"
	apiReq, err := api.BuildRequest(c.UserAgent, c.ContentType, method, url, nil)
	if err != nil {
		return result, err
	}
	var apiRes *http.Response
	var apiResBody []byte
	apiRes, apiResBody, err = api.Do(c.HttpClient, apiReq)
	if err != nil {
		return result, err
	}
	if apiRes.StatusCode != http.StatusOK {
		return result, fmt.Errorf("status %s returned from api call to %s", apiRes.Status, reduxedURL)
	}
	err = api.CheckContentType(c.ContentType, apiRes.Header["Content-Type"])
	if err != nil {
		return result, err
	}
	err = json.Unmarshal(apiResBody, &result)
	if err != nil {
		return result, fmt.Errorf("%v parsing response %s returned from api call to %s", err, string(apiResBody), reduxedURL)
	}
	if len(result.Items) != 1 {
		return result, fmt.Errorf("invalid response %s returned from api call to %s", string(apiResBody), reduxedURL)
	}
	if result.Items[0].Error == "1001" || result.Items[0].Error == "1002" {
		return result, ToInvalidSortCodeError(errors.New("invalid sort code"))
	}
	if result.Items[0].Error == "1003" || result.Items[0].Error == "1004" {
		return result, ToInvalidAccountNumberError(errors.New("invalid account number"))
	}
	if result.Items[0].Error != "" {
		return result, fmt.Errorf("invalid response %s returned from api call to %s", string(apiResBody), reduxedURL)
	}
	if !result.Items[0].IsCorrect {
		return result, ToInvalidError(errors.New("invalid bank account details"))
	}
	if !result.Items[0].IsDirectDebitCapable {
		return result, ToNotDirectDebitCapableError(errors.New("bank account is not direct debit capable"))
	}
	return result, nil
}
