package pcapredict

import (
	"fmt"

	"bitbucket.org/homemade/polyphene/providers"
)

type RecoverableError interface {
	error
	RecoverableErrorMessage() string
}

type recoverableError struct {
	err error
	msg string
}

func (e recoverableError) Error() string {
	return e.err.Error()
}

func (e recoverableError) RecoverableErrorMessage() string {
	return e.msg
}

func ToInvalidError(err error) RecoverableError {
	return recoverableError{
		err: err,
		msg: "Invalid",
	}
}

func ToInvalidAccountNumberError(err error) RecoverableError {
	return recoverableError{
		err: err,
		msg: "InvalidAccountNumber",
	}
}

func ToInvalidSortCodeError(err error) RecoverableError {
	return recoverableError{
		err: err,
		msg: "InvalidSortCode",
	}
}

func ToNotDirectDebitCapableError(err error) RecoverableError {
	return recoverableError{
		err: err,
		msg: "NotDirectDebitCapable",
	}
}

func IsRecoverableError(err error) (bool, string) {
	if re, ok := err.(RecoverableError); ok && re.RecoverableErrorMessage() != "" {
		re := fmt.Sprintf("%s:%s", providers.PCAPredict.String(), re.RecoverableErrorMessage())
		return true, re
	}
	return false, ""
}
