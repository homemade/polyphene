package pcapredict

import (
	"bitbucket.org/homemade/polyphene/commands"
	"bitbucket.org/homemade/polyphene/providers"
)

var (
	ConfigKey_APIKey = providers.PCAPredict.String() + ".APIKey"
	ConfigKey_Params = providers.PCAPredict.String() + commands.ConfigKeySuffix_Params
	ConfigKey_Result = providers.PCAPredict.String() + commands.ConfigKeySuffix_Result
)
