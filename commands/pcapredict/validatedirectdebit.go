package pcapredict

import (
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/api"
	"bitbucket.org/homemade/polyphene/mapping"
)

type ValidateDirectDebit struct {
	client                *Client
	paramsMappingTemplate mapping.MappingTemplate
	mapResults            bool // result mapping is optional
	resultMappingTemplate mapping.MappingTemplate
}

func (v *ValidateDirectDebit) Configure(cfg map[string]string) error {
	// to create a pcapredict api client we require a single api key
	apiKeys, err := polyphene.GetRequiredConfigValues(cfg, ConfigKey_APIKey)
	if err != nil {
		return err
	}

	v.client = &Client{
		UserAgent:   "Polyphene",
		ContentType: "application/json",
		HttpClient:  api.DefaultHTTPClient(),
		APIKey:      apiKeys[0],
	}
	// create toml mapping template for unmarshalling params
	var params []string
	params, err = polyphene.GetRequiredConfigValues(cfg, ConfigKey_Params)
	if err != nil {
		return err
	}
	v.paramsMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Params, params[0])
	if err != nil {
		return err
	}
	// result mapping is optional
	if r, ok := cfg[ConfigKey_Result]; ok && r != "" {
		v.resultMappingTemplate, err = mapping.CreateTOMLMappingTemplate(ConfigKey_Result, r)
		if err != nil {
			return err
		}
		v.mapResults = true
	}
	return nil
}

func (v ValidateDirectDebit) Invoke(event polyphene.Event) (polyphene.Outcome, error) {
	var validateDirectDebitParams ValidateDirectDebitParams
	_, err := v.paramsMappingTemplate.ExecuteAndUnmarshal(event.ModelValues(), &validateDirectDebitParams)
	if err != nil {
		return nil, err
	}
	var recoverableErrors []string
	var response ValidateDirectDebitResult
	result := make(map[string]interface{})
	response, err = v.client.ValidateDirectDebit(validateDirectDebitParams)
	if err != nil {
		if recoverable, re := IsRecoverableError(err); recoverable {
			recoverableErrors = append(recoverableErrors, re)
		} else {
			return nil, fmt.Errorf("validate direct debit %#v failed with error %v", validateDirectDebitParams, err)
		}
	}
	if v.mapResults {
		_, err = v.resultMappingTemplate.ExecuteAndUnmarshal(response, &result)
		if err != nil {
			return nil, err
		}
	}
	return polyphene.CreateOutcome(result, recoverableErrors), nil
}
