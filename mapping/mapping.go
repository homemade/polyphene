package mapping

import (
	"bytes"
	"fmt"
	"text/template"

	"github.com/BurntSushi/toml"
	"github.com/Masterminds/sprig"
)

type MappingTemplate interface {
	Name() string
	ExecuteAndUnmarshal(data interface{}, v interface{}) (executedTemplate string, err error)
}

type tomlMappingTemplate struct {
	name string
	tmpl *template.Template
}

func (t tomlMappingTemplate) Name() string {
	return t.name
}

func (t tomlMappingTemplate) ExecuteAndUnmarshal(data interface{}, v interface{}) (executedTemplate string, err error) {
	var mappings bytes.Buffer
	err = t.tmpl.Execute(&mappings, data)
	if err != nil {
		err = fmt.Errorf("error executing toml mapping template %s %v", t.name, err)
		return
	}
	b := mappings.Bytes()
	executedTemplate = string(b)
	err = toml.Unmarshal(b, v)
	if err != nil {
		err = fmt.Errorf("error unmarshalling toml mapping template %s %v %s", t.name, err, executedTemplate)
		return
	}
	return
}

func CreateTOMLMappingTemplate(name string, tomlTemplate string) (MappingTemplate, error) {
	// NOTE: we augment the standard Go template functions with Sprig - see http://masterminds.github.io/sprig/
	tmpl, err := template.New(name).Funcs(sprig.TxtFuncMap()).Parse(tomlTemplate)
	if err != nil {
		return nil, fmt.Errorf("error parsing toml mapping template %s %v", name, err)
	}
	return tomlMappingTemplate{name, tmpl}, nil
}
