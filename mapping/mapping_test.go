// go test -v kew_foundation_platform/polyphene/mapping
package mapping

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func test(t *testing.T, tf string) {
	f := strings.Replace(tf, "request.json", "params.toml", 1)
	if !isRequest(tf) {
		f = strings.Replace(tf, "response.json", "result.toml", 1)
	}
	b, err := ioutil.ReadFile(f)
	require.Nil(t, err)
	require.NotNil(t, b)
	var mt MappingTemplate
	mt, err = CreateTOMLMappingTemplate(f, string(b))
	require.Nil(t, err)
	require.NotNil(t, mt)
	b, err = ioutil.ReadFile(tf)
	require.Nil(t, err)
	require.NotNil(t, b)
	d := make(map[string]interface{})
	err = json.Unmarshal(b, &d)
	require.Nil(t, err)
	require.NotZero(t, len(d))
	v := make(map[string]interface{})
	_, err = mt.ExecuteAndUnmarshal(d, &v)
	require.Nil(t, err)
	require.NotZero(t, len(v))
	t.Logf("%s %#v", tf, v)
}

func isRequest(filename string) bool {
	return filepath.Ext(filename) == ".json" &&
		filepath.Ext(strings.Replace(filename, ".json", "", 1)) == ".request"
}

func TestMappings(t *testing.T) {
	var testRequests []string
	// collect the test requests as setup in the testdata directory
	fs, err := ioutil.ReadDir("testdata")
	require.Nil(t, err)
	require.NotZero(t, len(fs))
	for _, f := range fs {
		if !f.IsDir() && isRequest(f.Name()) {
			testRequests = append(testRequests, f.Name())
		}
	}
	// run the tests
	for _, tr := range testRequests {
		tf := filepath.Join("testdata", tr)
		test(t, tf)
		test(t, strings.Replace(tf, "request.json", "response.json", 1))
	}
}
