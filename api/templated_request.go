package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"text/template"

	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/xml"
)

var M *minify.M

func init() {
	M = minify.New()
	M.AddFunc("text/xml", xml.Minify)
}

// RequestTemplate contains a *template.Template used to build API requests
type RequestTemplate struct {
	Config      map[string]string
	Template    *template.Template
	UserAgent   string
	ContentType string
	Method      string
	URL         string
}

// BuildTemplatedRequest assembles a templated API request ready for transport
func BuildTemplatedRequest(tmpl RequestTemplate, data interface{}) (req *http.Request, body []byte, err error) {
	v := struct {
		Config map[string]string
		Data   interface{}
	}{
		tmpl.Config,
		data,
	}
	var reqBody bytes.Buffer
	err = tmpl.Template.Execute(&reqBody, v)
	if err != nil {
		return nil, nil, fmt.Errorf("error executing template %s, %v", tmpl.Template.Name(), err)
	}

	if strings.HasPrefix(tmpl.ContentType, "application/json") {
		var jsonBody bytes.Buffer
		err = json.Compact(&reqBody, reqBody.Bytes())
		if err != nil {
			return nil, nil, fmt.Errorf("error in json request body generated from template %s, %s, %v", tmpl.Template.Name(), reqBody.String(), err)
		}
		body = jsonBody.Bytes()
		req, err = BuildRequest(tmpl.UserAgent, tmpl.ContentType, tmpl.Method, tmpl.URL, &jsonBody)
		return
	}

	if strings.HasPrefix(tmpl.ContentType, "text/xml") ||
		strings.HasPrefix(tmpl.ContentType, "application/xml") ||
		strings.HasPrefix(tmpl.ContentType, "application/soap+xml") {
		var xmlBody bytes.Buffer
		err = M.Minify("text/xml", &xmlBody, &reqBody)
		if err != nil {
			return nil, nil, fmt.Errorf("error in xml request body generated from template %s, %s, %v", tmpl.Template.Name(), reqBody.String(), err)
		}
		body = xmlBody.Bytes()
		req, err = BuildRequest(tmpl.UserAgent, tmpl.ContentType, tmpl.Method, tmpl.URL, &xmlBody)
		return
	}

	body = reqBody.Bytes()
	req, err = BuildRequest(tmpl.UserAgent, tmpl.ContentType, tmpl.Method, tmpl.URL, &reqBody)
	return
}
