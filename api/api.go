// Package api provides common utility functions for working with HTTP(S) APIs
package api

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"
)

// DefaultHTTPClient returns a http client suitable for api calls with a timeout of 20 seconds
// see https://medium.com/@nate510/don-t-use-go-s-default-http-client-4804cb19f779
func DefaultHTTPClient() *http.Client {
	return &http.Client{Timeout: time.Second * 20}
}

// BuildRequest assembles an API request ready for transport
func BuildRequest(userAgent string, contentType string, method string, url string, reqBody io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, reqBody)
	if err != nil {
		return nil, fmt.Errorf("error building %s request to %s %v", method, url, err)
	}
	if userAgent != "" {
		req.Header.Set("User-Agent", userAgent)
	}
	if contentType != "" && contentType != "*/*" {
		req.Header.Set("Content-Type", contentType)
	}
	if contentType != "" {
		req.Header.Set("Accept", contentType)
	}

	return req, nil
}

// Do transports a single API request
func Do(client *http.Client, req *http.Request) (res *http.Response, readBody []byte, err error) {
	res, err = client.Do(req)
	if err != nil {
		return res, readBody, err
	}
	// Defer closing of underlying connection so it can be re-used
	defer func() {
		if req.Method != http.MethodHead && res != nil && (res.ContentLength > 0 || res.ContentLength == -1) { // -1 represents unknown content length
			res.Body.Close()
		}
	}()
	if req.Method != http.MethodHead && res != nil && (res.ContentLength > 0 || res.ContentLength == -1) { // -1 represents unknown content length
		readBody, err = ioutil.ReadAll(res.Body)
	}
	if err != nil {
		return res, readBody, err
	}
	return res, readBody, nil
}

type batchedRequest struct {
	Sequence int
	Request  *http.Request
}

// DoBatch transports a sequence of API requests concurrently
func DoBatch(client *http.Client, reqs []*http.Request) (resps []*http.Response, readBodies [][]byte, errs []error) {
	z := len(reqs)

	// Initialise our results
	resps = make([]*http.Response, z)
	readBodies = make([][]byte, z)
	errs = make([]error, z)

	// Setup a buffered channel to queue up the requests for processing
	batchedRequests := make(chan batchedRequest, z)
	for i := 0; i < z; i++ {
		batchedRequests <- batchedRequest{i, reqs[i]}
	}
	// Close the channel - nothing else is sent to it
	close(batchedRequests)

	// Setup a wait group so we know when all the batchedRequests have been processed
	var wg sync.WaitGroup
	wg.Add(z)

	// Start our individual goroutines to process each batchedRequest
	for i := 0; i < z; i++ {
		go func() {
			defer wg.Done()
			// Process a request
			batchedReq := <-batchedRequests
			resps[batchedReq.Sequence], readBodies[batchedReq.Sequence], errs[batchedReq.Sequence] = Do(client, batchedReq.Request)
		}()
	}

	wg.Wait()
	return

}

// CheckContentType checks response content types against an expected content type
func CheckContentType(expectedContentType string, responseContentTypes []string) error {
	receivedContentTypes := ""
	for _, c := range responseContentTypes {
		receivedContentTypes += " " + strings.ToLower(c)
	}
	if receivedContentTypes != "" && (!strings.Contains(receivedContentTypes, expectedContentType)) {
		return fmt.Errorf("unexpected response content type expected %s but received %s", expectedContentType, receivedContentTypes)
	}
	return nil
}
