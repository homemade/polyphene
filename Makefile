#!make
include .env
export $(shell sed 's/=.*//' .env)

HAS_GLIDE := $(shell command -v glide;)

.PHONY: test
test:
	go test -v bitbucket.org/homemade/polyphene/handlers

.PHONY: setup
setup:
ifndef HAS_GLIDE
	go get -u github.com/Masterminds/glide
endif
	glide install

.PHONY: Braintree
Braintree:
	go test -v -run=Braintree bitbucket.org/homemade/polyphene/handlers

.PHONY: Mailjet
Mailjet:
	go test -v -run=Mailjet bitbucket.org/homemade/polyphene/handlers

.PHONY: PCAPredict
PCAPredict:
	go test -v -run=PCAPredict bitbucket.org/homemade/polyphene/handlers

.PHONY: GoogleAnalytics
GoogleAnalytics:
	go test -v -run=GoogleAnalytics bitbucket.org/homemade/polyphene/handlers

.PHONY: Silverbear
Silverbear:
	go test -v -run=Silverbear bitbucket.org/homemade/polyphene/handlers
