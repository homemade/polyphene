package polyphene

type Event interface {
	Name() string
	ModelName() string
	ModelValues() map[string]interface{}
}

type event struct {
	name        string
	modelName   string
	modelValues map[string]interface{}
}

func (e event) Name() string {
	return e.name
}

func (e event) ModelName() string {
	return e.modelName
}

func (e event) ModelValues() map[string]interface{} {
	return e.modelValues
}

func CreateEvent(name string, modelName string, modelValues map[string]interface{}) Event {
	return event{name, modelName, modelValues}
}
