// Package polyphene is a framework for building business logic event handlers compatible with Sepia.
// It is named after the cephalopods remarkable polyphenism  https://en.wikipedia.org/wiki/Cuttlefish#Communication.
package polyphene

import (
	"fmt"
)

type Logger interface {
	Log(string, ...interface{})
}

// BusinessLogicHandler defines the polyphene business logic event handler.
type BusinessLogicHandler interface {
	HandleEvent(event Event) (Outcome, error)
}

// Command defines a template method for the handler to carry out business logic.
// https://en.wikipedia.org/wiki/Template_method_pattern
type Command interface {
	Configure(cfg map[string]string) error
	Invoke(event Event) (Outcome, error)
}

// Outcome defines the outcome of executing the polyphene command.
type Outcome interface {
	Result() map[string]interface{}
	RecoverableErrors() []string
}

type polypheneOutcome struct {
	result            map[string]interface{}
	recoverableErrors []string
}

func (p polypheneOutcome) Result() map[string]interface{} {
	return p.result
}

func (p polypheneOutcome) RecoverableErrors() []string {
	return p.recoverableErrors
}

func CreateOutcome(result map[string]interface{}, recoverableErrors []string) Outcome {
	return polypheneOutcome{result, recoverableErrors}
}

func GetRequiredConfigValues(cfg map[string]string, keys ...string) ([]string, error) {
	var values []string
	var missing []string
	for _, k := range keys {
		if v, ok := cfg[k]; ok && v != "" {
			values = append(values, v)
		} else {
			missing = append(missing, k)
		}
	}
	if len(missing) > 0 {
		return values, fmt.Errorf("missing required config values for keys %v", missing)
	}
	return values, nil
}

type polypheneHandler struct {
	logger Logger
	cmd    Command
}

func (p polypheneHandler) HandleEvent(event Event) (Outcome, error) {
	return p.cmd.Invoke(event)
}

func CreateBusinessLogicHandler(logger Logger, cfg map[string]string, cmd Command) (BusinessLogicHandler, error) {
	err := cmd.Configure(cfg)
	if err != nil {
		return nil, err
	}
	return polypheneHandler{logger, cmd}, nil
}
