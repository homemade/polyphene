// go get golang.org/x/tools/cmd/stringer
// go generate bitbucket.org/homemade/polyphene/providers
package providers

import "fmt"

// Provider represents a service provider
//go:generate stringer -type=Provider
type Provider int

const (
	_ Provider = iota
	Braintree
	Mailjet
	SendGrid
	Stripe
	PCAPredict
	GoogleAnalytics
	Silverbear
)

var ProviderMap = map[string]Provider{
	Braintree.String():       Braintree,
	Mailjet.String():         Mailjet,
	SendGrid.String():        SendGrid,
	Stripe.String():          Stripe,
	PCAPredict.String():      PCAPredict,
	GoogleAnalytics.String(): GoogleAnalytics,
	Silverbear.String():      Silverbear,
}

func MustMatch(provider string) (Provider, error) {
	var result Provider
	var ok bool
	if result, ok = ProviderMap[provider]; ok {
		return result, nil
	}
	return result, fmt.Errorf("no matching provider %s", provider)
}
