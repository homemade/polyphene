# Configuration

Create an `.env` file and fill in the blanks

```
MAILJET_APIKEY_PUBLIC=
MAILJET_APIKEY_PRIVATE=
BRAINTREE_ENVIRONMENT=sandbox
BRAINTREE_MERCHANT=
BRAINTREE_PUBLIC_KEY=
BRAINTREE_PRIVATE_KEY=
BRAINTREE_TOKENIZATION_KEY=
PCAPREDICT_API_KEY=
SILVERBEAR_ENDPOINT=
SILVERBEAR_USERNAME=
SILVERBEAR_PASSWORD=
SILVERBEAR_GUID_CUR=
SILVERBEAR_GUID_BUS_ORG=
SILVERBEAR_GUID_VAT=
SILVERBEAR_GUID_PRD_DON_REG=
SILVERBEAR_GUID_PLS_DON_REG=
SILVERBEAR_GUID_PRD_DON_SIN=
SILVERBEAR_GUID_PLS_DON_SIN=
```

Or if on Windows maybe use a `env.bat` file and run it before the tests

```
SET MAILJET_APIKEY_PUBLIC=
SET MAILJET_APIKEY_PRIVATE=
SET BRAINTREE_ENVIRONMENT=sandbox
SET BRAINTREE_MERCHANT=
SET BRAINTREE_PUBLIC_KEY=
SET BRAINTREE_PRIVATE_KEY=
SET BRAINTREE_TOKENIZATION_KEY=
SET PCAPREDICT_API_KEY=
SET SILVERBEAR_ENDPOINT=
SET SILVERBEAR_USERNAME=
SET SILVERBEAR_PASSWORD=
SET SILVERBEAR_GUID_CUR=
SET SILVERBEAR_GUID_BUS_ORG=
SET SILVERBEAR_GUID_VAT=
SET SILVERBEAR_GUID_PRD_DON_REG=
SET SILVERBEAR_GUID_PLS_DON_REG=
SET SILVERBEAR_GUID_PRD_DON_SIN=
SET SILVERBEAR_GUID_PLS_DON_SIN=
```

# Run all tests

`make`

Or if on Windows `go test -v bitbucket.org/homemade/polyphene/handlers`

# Run specific tests

`make Braintree` Or `go test -v -run=Braintree bitbucket.org/homemade/polyphene/handlers`

`make Mailjet` Or `go test -v -run=Mailjet bitbucket.org/homemade/polyphene/handlers`

`make PCAPredict` Or `go test -v -run=PCAPredict bitbucket.org/homemade/polyphene/handlers`

`make GoogleAnalytics` Or `go test -v -run=GoogleAnalytics bitbucket.org/homemade/polyphene/handlers`

`make Silverbear` Or `go test -v -run=Silverbear bitbucket.org/homemade/polyphene/handlers`
