package logger

import (
	"fmt"
	"testing"
)

//TestLogger writes to the testing log instead of the stdout.
type TestLogger struct {
	T *testing.T
}

// Log sends a message to the unit test output (visible with go test -v)
func (l TestLogger) Log(message string, args ...interface{}) {
	l.T.Log(fmt.Sprintf(message, args...))
}
