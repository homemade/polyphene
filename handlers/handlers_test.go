package handlers

import (
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/commands"
	"bitbucket.org/homemade/polyphene/commands/braintree"
	"bitbucket.org/homemade/polyphene/commands/googleanalytics"
	"bitbucket.org/homemade/polyphene/commands/mailjet"
	"bitbucket.org/homemade/polyphene/commands/pcapredict"
	"bitbucket.org/homemade/polyphene/commands/silverbear"
	"bitbucket.org/homemade/polyphene/handlers/analytics"
	"bitbucket.org/homemade/polyphene/handlers/fulfilment"
	"bitbucket.org/homemade/polyphene/handlers/mail"
	"bitbucket.org/homemade/polyphene/handlers/payment"
	"bitbucket.org/homemade/polyphene/logger"
	"bitbucket.org/homemade/polyphene/providers"
)

func isRequest(filename string) bool {
	return filepath.Ext(filename) == ".json" &&
		filepath.Ext(strings.Replace(filename, ".json", "", 1)) == ".request"
}

func test(t *testing.T, filterOnProvider string) {
	var testRequests []string
	// collect the test requests as setup in the mapping testdata directory
	fs, err := ioutil.ReadDir("../mapping/testdata")
	require.Nil(t, err)
	require.NotZero(t, len(fs))
	for _, f := range fs {
		if !f.IsDir() && isRequest(f.Name()) {
			testRequests = append(testRequests, f.Name())
		}
	}
	// add any env vars required to run the tests
	cfg := make(map[string]string)
	cfg[mailjet.ConfigKey_PublicAPIKey] = os.Getenv("MAILJET_APIKEY_PUBLIC")
	cfg[mailjet.ConfigKey_PrivateAPIKey] = os.Getenv("MAILJET_APIKEY_PRIVATE")

	cfg[braintree.ConfigKey_Environment] = os.Getenv("BRAINTREE_ENVIRONMENT")
	cfg[braintree.ConfigKey_Merchant] = os.Getenv("BRAINTREE_MERCHANT")
	cfg[braintree.ConfigKey_PublicKey] = os.Getenv("BRAINTREE_PUBLIC_KEY")
	cfg[braintree.ConfigKey_PrivateKey] = os.Getenv("BRAINTREE_PRIVATE_KEY")

	cfg[pcapredict.ConfigKey_APIKey] = os.Getenv("PCAPREDICT_API_KEY")

	// silverbear environment
	cfg[silverbear.ConfigKey_SilverbearEndpoint] = os.Getenv("SILVERBEAR_ENDPOINT")
	cfg[silverbear.ConfigKey_SilverbearUsername] = os.Getenv("SILVERBEAR_USERNAME")
	cfg[silverbear.ConfigKey_SilverbearPassword] = os.Getenv("SILVERBEAR_PASSWORD")
	// silverbear guid for currency
	cfg[silverbear.ConfigKey_SilverbearGUIDCur] = os.Getenv("SILVERBEAR_GUID_CUR")
	// silverbear guid for business organisation
	cfg[silverbear.ConfigKey_SilverbearGUIDBusOrg] = os.Getenv("SILVERBEAR_GUID_BUS_ORG")

	// run google analytics tests in debug mode
	cfg[googleanalytics.ConfigKey_DebugMode] = "true"

	// setup a test logger
	log := logger.TestLogger{T: t}

	for _, tr := range testRequests {
		// extract provider (this is the prefix)
		parts := strings.Split(tr, ".")
		require.NotZero(t, len(parts))
		var provider providers.Provider
		provider, err = providers.MustMatch(parts[0])
		require.Nil(t, err)
		if filterOnProvider != "" {
			if filterOnProvider != provider.String() {
				continue
			}
		}
		// request file path
		requestFile := filepath.Join("../mapping/testdata", tr)
		// read test params mapping
		paramsFile := strings.Replace(requestFile, "request.json", "params.toml", 1)
		var b []byte
		b, err = ioutil.ReadFile(paramsFile)
		require.Nil(t, err)
		require.NotNil(t, b)
		cfg[provider.String()+commands.ConfigKeySuffix_Params] = string(b)
		// read test result mapping
		resultFile := strings.Replace(requestFile, "request.json", "result.toml", 1)
		b, err = ioutil.ReadFile(resultFile)
		require.Nil(t, err)
		require.NotNil(t, b)
		cfg[provider.String()+commands.ConfigKeySuffix_Result] = string(b)
		t.Logf("testing %s ...\n", requestFile)
		// setup handler
		var handler polyphene.BusinessLogicHandler
		if strings.Contains(requestFile, ".SendTemplatedEmail.") {
			handler, err = mail.SendTemplatedEmailHandler(log, cfg, provider)
		}
		if strings.Contains(requestFile, ".CreateTransaction.") {
			handler, err = payment.CreateTransactionHandler(log, cfg, provider)
		}
		if strings.Contains(requestFile, ".ValidateDirectDebit.") {
			handler, err = payment.ValidateDirectDebitHandler(log, cfg, provider)
		}
		if strings.Contains(requestFile, ".TrackPayment.") {
			handler, err = analytics.TrackPaymentHandler(log, cfg, provider)
		}
		if strings.Contains(requestFile, ".ValidateDirectDebit.") {
			handler, err = payment.ValidateDirectDebitHandler(log, cfg, provider)
		}
		if strings.Contains(requestFile, ".DonateByDirectDebit.") {
			handler, err = fulfilment.DonateByDirectDebitHandler(log, cfg, provider)
		}
		require.Nil(t, err)
		require.NotNil(t, handler)
		// read test request data
		b, err = ioutil.ReadFile(requestFile)
		require.Nil(t, err)
		require.NotNil(t, b)
		d := make(map[string]interface{})
		err = json.Unmarshal(b, &d)
		require.Nil(t, err)
		require.NotZero(t, len(d))
		// handle event
		outcome, err := handler.HandleEvent(polyphene.CreateEvent("test", "donation", d))
		require.Nil(t, err)
		require.NotNil(t, outcome)
		t.Logf("outcome %#v\n", outcome)
		// to aid debugging decode base64 encoded ga_payment_notification_response
		if strings.Contains(requestFile, ".SendPaymentNotification.") && provider == providers.GoogleAnalytics {
			if enc, ok := outcome.Result()["ga_payment_notification_response"]; ok {
				var decoded []byte
				decoded, err = base64.StdEncoding.DecodeString(enc.(string))
				require.Nil(t, err)
				t.Logf("decoded ga_payment_notification_response %s\n", string(decoded))
			}
		}
	}
}

func TestBraintree(t *testing.T) {
	test(t, providers.Braintree.String())
}

func TestMailjet(t *testing.T) {
	test(t, providers.Mailjet.String())
}

func TestPCAPredict(t *testing.T) {
	test(t, providers.PCAPredict.String())
}

func TestGoogleAnalytics(t *testing.T) {
	test(t, providers.GoogleAnalytics.String())
}

func TestSilverbear(t *testing.T) {
	test(t, providers.Silverbear.String())
}
