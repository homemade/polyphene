package fulfilment

import (
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/commands/silverbear"
	"bitbucket.org/homemade/polyphene/providers"
)

func DonateByDirectDebitHandler(logger polyphene.Logger, cfg map[string]string, provider providers.Provider) (polyphene.BusinessLogicHandler, error) {
	if provider != providers.Silverbear {
		return nil, fmt.Errorf("provider %v is unsupported for the DonateByDirectDebitHandler", provider)
	}
	// other providers to be added here :)
	return polyphene.CreateBusinessLogicHandler(logger, cfg, &silverbear.DonateByDirectDebit{})

}
