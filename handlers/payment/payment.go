package payment

import (
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/commands/braintree"
	"bitbucket.org/homemade/polyphene/commands/pcapredict"
	"bitbucket.org/homemade/polyphene/providers"
)

func CreateTransactionHandler(logger polyphene.Logger, cfg map[string]string, provider providers.Provider) (polyphene.BusinessLogicHandler, error) {
	if provider != providers.Braintree {
		return nil, fmt.Errorf("provider %v is unsupported for the CreateTransactionHandler", provider)
	}
	// other providers to be added here :)
	return polyphene.CreateBusinessLogicHandler(logger, cfg, &braintree.CreateTransaction{})
}

func ValidateDirectDebitHandler(logger polyphene.Logger, cfg map[string]string, provider providers.Provider) (polyphene.BusinessLogicHandler, error) {
	if provider != providers.PCAPredict {
		return nil, fmt.Errorf("provider %v is unsupported for the ValidateDirectDebitHandler", provider)
	}
	// other providers to be added here :)
	return polyphene.CreateBusinessLogicHandler(logger, cfg, &pcapredict.ValidateDirectDebit{})

}
