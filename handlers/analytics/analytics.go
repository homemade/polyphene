package analytics

import (
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/commands/googleanalytics"
	"bitbucket.org/homemade/polyphene/providers"
)

func TrackPaymentHandler(logger polyphene.Logger, cfg map[string]string, provider providers.Provider) (polyphene.BusinessLogicHandler, error) {
	if provider != providers.GoogleAnalytics {
		return nil, fmt.Errorf("provider %v is unsupported for the TrackPayment", provider)
	}
	// other providers to be added here :)
	return polyphene.CreateBusinessLogicHandler(logger, cfg, &googleanalytics.TrackPayment{})

}
