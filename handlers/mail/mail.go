package mail

import (
	"fmt"

	"bitbucket.org/homemade/polyphene"
	"bitbucket.org/homemade/polyphene/commands/mailjet"
	"bitbucket.org/homemade/polyphene/providers"
)

func SendTemplatedEmailHandler(logger polyphene.Logger, cfg map[string]string, provider providers.Provider) (polyphene.BusinessLogicHandler, error) {
	if provider != providers.Mailjet {
		return nil, fmt.Errorf("provider %v is unsupported for the SendTemplatedEmailHandler", provider)
	}
	// other providers to be added here :)
	return polyphene.CreateBusinessLogicHandler(logger, cfg, &mailjet.SendTemplatedEmail{})
}
