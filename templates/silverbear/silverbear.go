package silverbear

import (
	"fmt"
	"text/template"

	"bitbucket.org/homemade/polyphene/api"

	uuid "github.com/satori/go.uuid"
)

const (
	SOAPHeader = `
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:a="http://www.w3.org/2005/08/addressing" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
    <s:Header>
        <a:Action s:mustUnderstand="1">%s</a:Action>
        <a:MessageID>%s</a:MessageID>
        <a:ReplyTo><a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address></a:ReplyTo>
        <a:To s:mustUnderstand="1">%s</a:To>
        <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <o:UsernameToken u:Id="_0">
                <o:Username>%s</o:Username>
                <o:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">%s</o:Password>
            </o:UsernameToken>
        </o:Security>
    </s:Header>
    <s:Body>`

	SOAPFooter = `
	</s:Body>
</s:Envelope>`

	Action_FindContactsByEmail          = "FindContactsByEmail"
	Action_RegisterUser                 = "RegisterUser"
	Action_CreateDirectDebitInstruction = "CreateDirectDebitInstruction"
	Action_GetDirectDebitInstructions   = "GetDirectDebitInstructions"
	Action_PurchaseProducts             = "PurchaseProducts"
)

func BuildSOAPTemplate(endpoint string, action string, username string, password string, body string) (*template.Template, error) {
	// The header and footer have the same format for the silverbear messages
	// - it is just the body that varies
	soapAction := fmt.Sprintf("http://tempuri.org/IMembershipWebService/%s", action)
	messageID := fmt.Sprintf("urn:uuid:%s", uuid.NewV4())
	header := fmt.Sprintf(SOAPHeader, soapAction, messageID, endpoint, username, password)
	return template.New(action).Parse(header + body + SOAPFooter)
}

func BuildRequestTemplates(useragent string, endpoint string, username string, password string, config map[string]string) (map[string]api.RequestTemplate, error) {
	requestTemplates := make(map[string]api.RequestTemplate)

	tmpl, err := FindContactsByEmail(endpoint, username, password)
	if err != nil {
		return requestTemplates, err
	}
	requestTemplates[Action_FindContactsByEmail] = api.RequestTemplate{
		Config:      config,
		Template:    tmpl,
		UserAgent:   useragent,
		ContentType: "application/soap+xml; charset=utf-8",
		Method:      "POST",
		URL:         endpoint,
	}

	tmpl, err = RegisterUser(endpoint, username, password)
	if err != nil {
		return requestTemplates, err
	}
	requestTemplates[Action_RegisterUser] = api.RequestTemplate{
		Config:      config,
		Template:    tmpl,
		UserAgent:   useragent,
		ContentType: "application/soap+xml; charset=utf-8",
		Method:      "POST",
		URL:         endpoint,
	}

	tmpl, err = CreateDirectDebitInstruction(endpoint, username, password)
	if err != nil {
		return requestTemplates, err
	}
	requestTemplates[Action_CreateDirectDebitInstruction] = api.RequestTemplate{
		Config:      config,
		Template:    tmpl,
		UserAgent:   useragent,
		ContentType: "application/soap+xml; charset=utf-8",
		Method:      "POST",
		URL:         endpoint,
	}

	tmpl, err = GetDirectDebitInstructions(endpoint, username, password)
	if err != nil {
		return requestTemplates, err
	}
	requestTemplates[Action_GetDirectDebitInstructions] = api.RequestTemplate{
		Config:      config,
		Template:    tmpl,
		UserAgent:   useragent,
		ContentType: "application/soap+xml; charset=utf-8",
		Method:      "POST",
		URL:         endpoint,
	}

	return requestTemplates, nil
}
