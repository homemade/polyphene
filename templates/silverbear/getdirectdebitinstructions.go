package silverbear

import (
	"text/template"
)

func GetDirectDebitInstructions(endpoint string, username string, password string) (*template.Template, error) {
	body := `
	<GetDirectDebitInstructions xmlns="http://tempuri.org/">
		<contactId>{{ .Data }}</contactId>
		<getContracts>true</getContracts>
		<getPaymentCollectionLines>true</getPaymentCollectionLines>
	</GetDirectDebitInstructions>`
	return BuildSOAPTemplate(endpoint, Action_GetDirectDebitInstructions, username, password, body)
}
