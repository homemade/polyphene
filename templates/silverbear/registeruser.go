package silverbear

import (
	"text/template"
)

func RegisterUser(endpoint string, username string, password string) (*template.Template, error) {
	body := `
	<RegisterUser xmlns="http://tempuri.org/">
		<contactDetails xmlns:b="http://schemas.datacontract.org/2004/07/Silverbear.WebService.MembershipApi.Contract.Partials" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
			<b:Addresses>
				<b:Address>
					<b:Address1>{{ if .Data.Address1 }}{{ .Data.Address1 }}{{ end }}</b:Address1>
					<b:Address2>{{ if .Data.Address2 }}{{ .Data.Address2 }}{{ end }}</b:Address2>
					<b:Address3>{{ if .Data.Address3 }}{{ .Data.Address3 }}{{ end }}</b:Address3>
					<b:AddressName i:nil="true"/>
					<b:City>{{ .Data.TownOrCity }}</b:City>
					<b:ContactName i:nil="true"/>
					<b:Country i:nil="true"/>
					<b:CountryId i:nil="true"/>
					<b:County i:nil="true"/>
					<b:PostalCode>{{ .Data.Postcode }}</b:PostalCode>
					<b:Type>Home</b:Type>
				</b:Address>
			</b:Addresses>
			<b:BusinessFax i:nil="true"/>
			<b:BusinessOrganisationId>{{ index .Config "Silverbear.GUIDBusOrg" }}</b:BusinessOrganisationId>
			<b:BusinessPhone i:nil="true"/>
			<b:ContactId>00000000-0000-0000-0000-000000000000</b:ContactId>
			<b:CreatePortalUser>false</b:CreatePortalUser>
			<b:CurrencyId>{{ index .Config "Silverbear.GUIDCur" }}</b:CurrencyId>
			<b:DateOfBirth i:nil="true"/>
			<b:DoNotEmail>false</b:DoNotEmail>
			<b:EducationPhone i:nil="true"/>
			<b:EmailAddresses>
				<b:EmailAddress>
					<b:Email>{{ .Data.EmailAddress }}</b:Email>
					<b:IsPreferred>true</b:IsPreferred>
					<b:Type>Home</b:Type>
				</b:EmailAddress>
			</b:EmailAddresses>
			<b:FirstName>{{ .Data.FirstName }}</b:FirstName>
			<b:Gender i:nil="true"/>
			<b:HomeFax i:nil="true"/>
			<b:HomePhone>{{ .Data.PhoneNumber }}</b:HomePhone>
			<b:JobTitle i:nil="true"/>
			<b:LastName>{{ .Data.LastName }}</b:LastName>
			<b:LinkedInUrl i:nil="true"/>
			<b:MiddleName i:nil="true"/>
			<b:MobilePhone i:nil="true"/>
			<b:Password i:nil="true"/>
			<b:PostNominals i:nil="true"/>
			<b:PreferredGroupId i:nil="true"/>
			<b:PreferredGroupName i:nil="true"/>
			<b:PreferredName i:nil="true"/>
			<b:PreferredRegionId i:nil="true"/>
			<b:PreferredRegionName i:nil="true"/>
			<b:Title>{{ .Data.Title }}</b:Title>
			<b:TitleInteger i:nil="true"/>
			<b:TitleString i:nil="true"/>
			<b:TwitterUrl i:nil="true"/>
			<b:WebsiteUrl i:nil="true"/>
		</contactDetails>
		<portalUsername>{{ .Data.EmailAddress }}</portalUsername>
	</RegisterUser>`
	return BuildSOAPTemplate(endpoint, Action_RegisterUser, username, password, body)
}
