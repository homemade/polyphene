package silverbear

import (
	"text/template"
)

func FindContactsByEmail(endpoint string, username string, password string) (*template.Template, error) {
	body := `
	<FindContactsByEmail xmlns="http://tempuri.org/">
		<emailAddress>{{ .Data.EmailAddress }}</emailAddress>
		<additionalFields xmlns:b="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"/>
	</FindContactsByEmail>
	`
	return BuildSOAPTemplate(endpoint, Action_FindContactsByEmail, username, password, body)
}
