package silverbear

import (
	"text/template"
)

func PurchaseProducts(endpoint string, username string, password string, timestamp string) (*template.Template, error) {
	// add timestamp using string formatting functions
	body := `
<PurchaseProducts xmlns="http://tempuri.org/">
	<purchaseDetails xmlns:b="http://schemas.datacontract.org/2004/07/Silverbear.Crm.Common.Entities.ProductPurchaseHelpers" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
		<b:AmountPaid>{{if eq .Data.DDGUID ""}}{{.Data.Params.Amount}}{{else}}0{{end}}</b:AmountPaid>
		<b:AuthenticationNumber i:nil="true"/>
		<b:BillingAddress i:nil="true" xmlns:c="http://schemas.datacontract.org/2004/07/Silverbear.Crm.Common.Entities.EntityFragments"/>
		<b:CreditCardReferenceNumber i:nil="true"/>
		<b:CrmCCRefNo i:nil="true"/>
		<b:OrderId i:nil="true"/>
		<b:PaymentDate>{{.Data.Timestamp}}</b:PaymentDate>
		<b:PaymentType>{{if eq .Data.DDGUID ""}}Standard{{else}}MonthlyDirectDebit{{end}}</b:PaymentType>
		<b:PurchaseOrderNumber i:nil="true"/>
		<b:ShippingAddress i:nil="true" xmlns:c="http://schemas.datacontract.org/2004/07/Silverbear.Crm.Common.Entities.EntityFragments"/>
		<b:ShippingProductId i:nil="true"/>
		<b:ack_3dSecureStatus i:nil="true"/>
		<b:ack_AVSCV2 i:nil="true"/>
		<b:ack_AddressResult i:nil="true"/>
		<b:ack_AddressStatus i:nil="true"/>
		<b:ack_CAVV i:nil="true"/>
		<b:ack_CV2Result i:nil="true"/>
		<b:ack_CardType i:nil="true"/>
		<b:ack_Last4Digits i:nil="true"/>
		<b:ack_PayerStatus i:nil="true"/>
		<b:ack_PostcodeResult i:nil="true"/>
		<b:ack_Status i:nil="true"/>
		<b:ack_StatusDetail i:nil="true"/>
		<b:ack_TxAuthNo i:nil="true"/>
		<b:ack_TxType i:nil="true"/>
		<b:ack_VPSSignature i:nil="true"/>
		<b:ack_VPSTxId i:nil="true"/>
		<b:ack_VendorTxCode i:nil="true"/>
		<b:ack_VpsProtocol i:nil="true"/>
		<b:request_AccountType i:nil="true"/>
		<b:request_AllowGiftAid i:nil="true"/>
		<b:request_Apply3DSecure i:nil="true"/>
		<b:request_ApplyAVSCV2 i:nil="true"/>
		<b:request_BillingAgreement i:nil="true"/>
		<b:request_Description i:nil="true"/>
		<b:request_NotificationURL i:nil="true"/>
		<b:request_Profile i:nil="true"/><b:request_TxType i:nil="true"/>
		<b:request_VPSProtocol i:nil="true"/><b:request_Vendor i:nil="true"/>
		<b:request_VendorTxCode i:nil="true"/><b:response_NextUrl i:nil="true"/>
		<b:response_ResponseStatus i:nil="true"/>
		<b:response_ResponseStatusDetail i:nil="true"/>
		<b:response_SecurityKey i:nil="true"/>
		<b:response_VPSProtocol i:nil="true"/>
		<b:response_VPSTxId i:nil="true"/>
		<b:BasketType>Order</b:BasketType>
		<b:BusinessOrganisationId>{{ index .Config "Silverbear.GUIDBusOrg" }}</b:BusinessOrganisationId>
		<b:CurrencyId>{{ index .Config "Silverbear.GUIDBusOrg" }}</b:CurrencyId>
		<b:CustomerId>{{ .Data.SBContact.GUID }}</b:CustomerId>
		<b:CustomerType>Contact</b:CustomerType>
		{{if eq .Data.DDGUID ""}}Standard{{else}}<b:DirectDebitInstructionId>{{.Data.DDGUID}}</b:DirectDebitInstructionId>{{end}}
		<b:PaymentType>{{if eq .Data.DDGUID ""}}Standard{{else}}MonthlyDirectDebit{{end}}</b:PaymentType>
		{{if eq .Data.DDGUID ""}}<b:DirectDebitInstructionId i:nil="true"/>{{else}}<b:DirectDebitInstructionId>{{.Data.DDGUID}}</b:DirectDebitInstructionId>{{end}}
		<b:PriceListId>{{if eq .Data.DDGUID ""}}{{ index .Config "Silverbear.GUIDPlsDonReg" }}{{else}}{{ index .Config "Silverbear.GUIDPlsDonSin" }}{{end}}</b:PriceListId>
		<b:PriorityOrder i:nil="true"/>
		<b:ProductDetails>
			<b:ProductPurchaseDetails>
				<b:Discount i:nil="true"/>
				<b:EndDate i:nil="true"/>
				<b:IsChargeable>true</b:IsChargeable>
				<b:PriceCalculationType>PriceList</b:PriceCalculationType>
				<b:ProductAdditionsPurchaseDetails/>
				
				<b:ProductId>aa8ebac7-91ca-e511-80dd-0050569c2c8a</b:ProductId>
				
				<b:Quantity>1</b:Quantity>
				<b:StartDate i:nil="true"/>
				
				<b:UnitPrice>10</b:UnitPrice>
				<b:VatRateId>4d8187fc-e8b8-e411-80cc-005056a77c46</b:VatRateId>
				<b:VatRatePercentage>0</b:VatRatePercentage>
			
			</b:ProductPurchaseDetails>
		</b:ProductDetails>
		<b:PurchaseDescription i:nil="true"/>
		<b:ShippingDetails i:nil="true"/>
		<b:TransactionPaymentDetails>
			<b:AmountPaid>0</b:AmountPaid>
			<b:AuthenticationNumber i:nil="true"/>
			<b:BillingAddress i:nil="true" xmlns:c="http://schemas.datacontract.org/2004/07/Silverbear.Crm.Common.Entities.EntityFragments"/>
			<b:CreditCardReferenceNumber i:nil="true"/>
			<b:CrmCCRefNo i:nil="true"/>
			
			<b:OrderId>12345</b:OrderId>

			<b:PaymentDate>%s</b:PaymentDate>
			
			<b:PaymentType>MonthlyDirectDebit</b:PaymentType>
			<b:PurchaseOrderNumber i:nil="true"/>
			<b:ShippingAddress i:nil="true" xmlns:c="http://schemas.datacontract.org/2004/07/Silverbear.Crm.Common.Entities.EntityFragments"/>
			<b:ShippingProductId i:nil="true"/>
			<b:ack_3dSecureStatus i:nil="true"/>
			<b:ack_AVSCV2 i:nil="true"/>
			<b:ack_AddressResult i:nil="true"/>
			<b:ack_AddressStatus i:nil="true"/>
			<b:ack_CAVV i:nil="true"/>
			<b:ack_CV2Result i:nil="true"/>
			<b:ack_CardType i:nil="true"/>
			<b:ack_Last4Digits i:nil="true"/>
			<b:ack_PayerStatus i:nil="true"/>
			<b:ack_PostcodeResult i:nil="true"/>
			<b:ack_Status i:nil="true"/>
			<b:ack_StatusDetail i:nil="true"/>
			<b:ack_TxAuthNo i:nil="true"/>
			<b:ack_TxType i:nil="true"/>
			<b:ack_VPSSignature i:nil="true"/>
			<b:ack_VPSTxId i:nil="true"/>
			<b:ack_VendorTxCode i:nil="true"/>
			<b:ack_VpsProtocol i:nil="true"/>
			<b:request_AccountType i:nil="true"/>
			<b:request_AllowGiftAid i:nil="true"/>
			<b:request_Apply3DSecure i:nil="true"/>
			<b:request_ApplyAVSCV2 i:nil="true"/>
			<b:request_BillingAgreement i:nil="true"/>
			<b:request_Description i:nil="true"/>
			<b:request_NotificationURL i:nil="true"/>
			<b:request_Profile i:nil="true"/>
			<b:request_TxType i:nil="true"/>
			<b:request_VPSProtocol i:nil="true"/>
			<b:request_Vendor i:nil="true"/>
			<b:request_VendorTxCode i:nil="true"/>
			<b:response_NextUrl i:nil="true"/>
			<b:response_ResponseStatus i:nil="true"/>
			<b:response_ResponseStatusDetail i:nil="true"/>
			<b:response_SecurityKey i:nil="true"/>
			<b:response_VPSProtocol i:nil="true"/>
			<b:response_VPSTxId i:nil="true"/>
		</b:TransactionPaymentDetails>
	</purchaseDetails>
	<existingPmOpportunityId i:nil="true" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"/>
</PurchaseProducts>`
	return BuildSOAPTemplate(endpoint, Action_CreateDirectDebitInstruction, username, password, body)
}
