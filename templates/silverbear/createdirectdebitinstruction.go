package silverbear

import (
	"text/template"
)

func CreateDirectDebitInstruction(endpoint string, username string, password string) (*template.Template, error) {
	body := `
	<CreateDirectDebitInstruction xmlns="http://tempuri.org/">
		<instruction xmlns:b="http://schemas.datacontract.org/2004/07/Silverbear.Crm.Common.Entities.ProductPurchaseHelpers" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
			<b:BankAddressDetails>
				<b:AccountName>{{ .Data.Params.BankDetails.AccountName }}</b:AccountName>
				<b:AccountNumber>{{ .Data.Params.BankDetails.AccountNumber }}</b:AccountNumber>
				<b:AddressBranch i:nil="true"/>
				<b:AddressCity>{{ .Data.Params.BankDetails.BankAddressCity }}</b:AddressCity>
				<b:AddressLine1>{{ .Data.Params.BankDetails.BankAddressLine1 }}</b:AddressLine1>
				<b:AddressLine2>{{ .Data.Params.BankDetails.BankAddressLine2 }}</b:AddressLine2>
				<b:AddressPostalCode>{{ .Data.Params.BankDetails.BankPostcode }}</b:AddressPostalCode>
				<b:BankName>{{ .Data.Params.BankDetails.BankName }}</b:BankName>
				<b:SortCode>{{ .Data.Params.BankDetails.SortCode }}</b:SortCode>
			</b:BankAddressDetails>
			<b:BusinessOrganisationId>{{ index .Config "Silverbear.GUIDBusOrg" }}</b:BusinessOrganisationId>
			<b:CustomerId>{{ .Data.SBContact.GUID }}</b:CustomerId>
			<b:CustomerType>Contact</b:CustomerType>
			<b:EmailAddress i:nil="true"/>
			<b:OrganisationName i:nil="true"/>
			<b:PayerAddressDetails xmlns:c="http://schemas.datacontract.org/2004/07/Silverbear.Crm.Common.Entities.EntityFragments">
				<c:AddressName i:nil="true"/>
				<c:City>{{ .Data.SBContact.ContactDetails.TownOrCity }}</c:City>
				<c:CompanyName i:nil="true"/>
				<c:Country i:nil="true"/>
				<c:CountryId i:nil="true"/>
				<c:EmailAddress i:nil="true"/>
				<c:EmailAddress2 i:nil="true"/>
				<c:FirstName>{{ .Data.SBContact.ContactDetails.FirstName }}</c:FirstName>
				<c:FullName i:nil="true"/>
				<c:HomeTelephone i:nil="true"/>
				<c:LastName>{{ .Data.SBContact.ContactDetails.LastName }}</c:LastName>
				<c:MobileTelephone i:nil="true"/>
				<c:Postcode>{{ .Data.SBContact.ContactDetails.Postcode }}</c:Postcode>
				<c:Region i:nil="true"/>
				<c:Street1>{{ .Data.SBContact.ContactDetails.Address1 }}</c:Street1>
				<c:Street2>{{ .Data.SBContact.ContactDetails.Address2 }}</c:Street2>
				<c:Street3>{{ .Data.SBContact.ContactDetails.Address3 }}</c:Street3>
				<c:Title i:nil="true"/>
				<c:TitleId i:nil="true"/>
			</b:PayerAddressDetails>
			<b:Telephone i:nil="true"/>
			<b:Title>{{ .Data.SBContact.ContactDetails.Title }}</b:Title>
		</instruction>
	</CreateDirectDebitInstruction>`
	return BuildSOAPTemplate(endpoint, Action_CreateDirectDebitInstruction, username, password, body)
}
